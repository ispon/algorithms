import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;


public  class MemCacheTimerCleanExpiredMapElements {
    Timer memCacheTimer;
    private static final long EXPIRED_TIME_IN_SEC = 5L;
    private static final Map<Double, ArrayList<Date>> memCacheTimerMap = new HashMap<Double, ArrayList<Date>>();
    
    public MemCacheTimerCleanExpiredMapElements(int periodSeconds){
        
        // Timer() - Creates a new timer. The associated thread does not run as daemon 
        this.memCacheTimer = new Timer();
        
        // Schedules the specified task for repeated fixed-delay execution
		
		// schedule(): Schedules the specified task for repeated fixed-delay execution, beginning after the specified delay.
		// Subsequent executions take place at approximately regular intervals separated by the specified period.
		// In fixed-delay execution, each execution is scheduled relative to the actual execution time of the previous execution.
		// If an execution is delayed for any reason (such as garbage collection or other background activity), subsequent executions will be delayed as well.
		// In the long run, the frequency of execution will generally be slightly lower than the reciprocal of the specified period (assuming the system clock underlying Object.wait(long) is accurate).
      
      this.memCacheTimer.schedule(new MemCacheTimerTask(this), 0L, periodSeconds * 1000L );
		
    }
  
  
  
  // Adding new element to map with current timestamp 
  public void addElementToMap(){
    Double randomDigit = Math.random();
    ArrayList<Date> memCacheList = memCacheTimerMap.getOrDefault(randomDigit, new ArrayList<Date>());
    memCacheList.add(new Date());

    memCacheTimerMap.put(randomDigit, memCacheList);
    this.memCacheLog("New element added: "+ randomDigit +"\n");
  }
  
  public void memCacheClearExpiredElementsFromMap(){
        // Date() - Allocates a Date object and initializes it so that it represents the time at which it was allocated, measured to the nearest millisecond.
		Date currentTime = new Date();
        Date actualExpiredTime = new Date();
        // if element time stamp and current time stamp difference is 5 second then delete element
		actualExpiredTime.setTime(currentTime.getTime() - EXPIRED_TIME_IN_SEC * 1000L);

        
    
        // size() - Returns the number of key-value mappings in this map. If the map contains more than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
		this.memCacheLog("memCacheTimerMap size:" + memCacheTimerMap.size());
    
        Iterator<Map.Entry<Double, ArrayList<Date>>> itr = memCacheTimerMap.entrySet().iterator();
        
        
        while(itr.hasNext()){
            Map.Entry<Double, ArrayList<Date>> entry = itr.next();
            
            ArrayList<Date> entryVal = entry.getValue();
            
            while(entryVal.size() > 0 && entryVal.get(0).compareTo(actualExpiredTime) < 0){
                this.memCacheLog("----------- Expired Element Deleted: " + entry.getKey());
                entryVal.remove(0);
            }
            
            if(entryVal.size() == 0){
                itr.remove();
            }
        }
		
  }
  
  private void memCacheLog(String logMessage){
    System.out.println(logMessage);
  }
}
