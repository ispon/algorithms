import java.util.TimerTask;

public class MemCacheTimerTask extends TimerTask{
    MemCacheTimerCleanExpiredMapElements memCacheTimerObj;
  
  
  public MemCacheTimerTask(MemCacheTimerCleanExpiredMapElements tempMemCacheTimerObj){
    this.memCacheTimerObj = tempMemCacheTimerObj;
    
  }
  
  public void run(){
    
//     // 
     this.memCacheTimerObj.memCacheClearExpiredElementsFromMap();
    
//     // 
     this.memCacheTimerObj.addElementToMap();
    
  }
}
